# SASS-convert

Þessi mappa inniheldur eftirfarandi:

* `.gitignore` - Stillingaskrá sem segir git hvaða skrár það á að sleppa að fylgjast með. 
* `gulpfile.js` - Lítið [gulp](http://gulpjs.com/) build forrit sem breytir öllum `*.scss` skrám í `src/` möppunni í `*.css` skrár `dist/` möppunni.
* `package.json` - Upplýsingar um hvaða npm (Node Package Manager) módúla/forrit þurfa að vera til staðar til að gulp forritið virki.
* `src/example.scss` - minimal SASS skrá til að gefa dæmi.
* `src/incl/partial.scss` - minimal partial (@import) SASS skrá.

## Install:

1. keyrðu skipunina `npm install -g gulp` (-g flaggið installar "gulp" sem command-line skipun)
2. afritaðu/vistaðu `.gitignore`, `gulpfile.js` og `package.json` yfir í rótina á verkefnamöppunni þinni.
3. `cd`-aðu inn í verkefnamöppuna í Terminal
4. keyrðu skipunina `npm install`
5. búðu til `src` möppu undir rótinni og settu allar `.scss` skrárnar þínar þangað inn.

## Notkun:

1. `cd`-aðu inn í verkefnamöppuna í Terminal
2. keyrðu skipunina `gulp`
3. Notaðu svo ritilinn þinn til að skrifa SCSS kóða og vista hann undir `src/`

Gulp task-ið býr sjálft til `dist` möppu við hliðina á `src` og setur þar inn `.css` skrárnar sem verða til.

Allar `*.scss` skrár sem liggja beint undir `src/` breytast í samsvarandi `.css` skrá undir `dist`.

Að auki fylgist gulp task-ið með `.scss` skrám í undirmöppum og uppfærir CSSið þegar þær breytast.

## Myndir, fontar, etc...

Allar asset skrár þurfa að geymast á stað þar sem CSS skrárnar geta vísað á þær.

Ef myndirnar eru t.d. geymdar í möppuni `images/` í rótinni á verkefnamöppunni (við hliðina á `src` og `dist`) þá þarf í CSSinu að vísa á þær með `url(../images/foo.png)`.

Einnig má geyma myndirnar í möppunni undir `dist/images/` til að losna við `../` úr myndaslóðunum.